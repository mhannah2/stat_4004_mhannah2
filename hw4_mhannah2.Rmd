---
title: "HW4_mhannah2"
author: "Hannah Most"
date: "2/18/2019"
output:
  pdf_document: default
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

library(dplyr)
library(readr)
library(httr)
library(readxl)
library(tidyverse)
library(stargazer)
library(knitr)
```

# Problem 3

```{r}
url1 <- "github.com/RMHogervorst/unicorns_on_unicycles/raw/master/observations.xlsx"
url2 <- "github.com/RMHogervorst/unicorns_on_unicycles/raw/master/sales.xlsx"

GET(url1, write_disk(tf <- tempfile(fileext = ".xlsx")))
GET(url2, write_disk(tf2 <- tempfile(fileext = ".xlsx")))

obs_raw <- read_xlsx(tf)
sales_raw <- read_xlsx(tf2)
sales_raw <- sales_raw[, -c(4, 5)]

sales_raw$population <- obs_raw$pop
sales_raw <- sales_raw[, -c(4, 5)]
sales_raw
```

# Problem 4

## Part A. Using dplyr

```{r}
byyear <- group_by(sales_raw, year)
byyear2 <- summarize(byyear, mean(bikes), mean(population))
byyear2
```

## Part B. Using a custom function

```{r}
bmean <- function(year, count){
  dframe <- cbind(year, count)
  aggregate(dframe[, 2], list(dframe[, 1]), mean)
}

bmean1 <- bmean(sales_raw$year, sales_raw$bikes)
bmean1

bmean2 <- bmean(sales_raw$year, sales_raw$population)
bmean2
```

## Part C. Compare methods

```{r}
p1 <- merge(bmean1, bmean2, by.x="Group.1", by.y="Group.1")
p2 <- merge(p1, byyear2, by.x="Group.1", by.y="year")
p2
kable(p2)
```

Do the summaries agree?  Does there appear to be a linear relationship between mean population and mean unicycle count by year?

# Problem 5: Sums of Squares 

```{r ss, echo=T, eval=F, include=T}
set.seed(12345)
    vec_length <- 1e6 
    y <- seq(from=1, to=100, length.out = vec_length) + rnorm(vec_length)
    time3A <- system.time({
        ybar <- mean(1:100)
        sos1 = 0
        for(i in length(y)){
          sos1 = (y[i] - ybar)^2
        }
    })
    time3B <- system.time({
        sos2 <- sum((y[i] - (mean(1:00)))^2)
    })
    time3A
    time3B
    sos1
    sos2
```

# Problem 6: Determine if there is a linear relationship between year and unicycle or unicorns

```{r eval=T, echo=T, include=T}
uni_lm <- lm(x.y~x.x, data = p2)
uni_lm
```

```{r eval=T, echo=T, include=T, results="asis"}
stargazer::stargazer(uni_lm,title = "Unicycles vs Unicorns", style = "qje", header=F)

cor(p2$x.x, p2$x.y)
```

I assume a linear relationship because the correlation coefficient is .9928, which is VERY close to 1.
---
title: "HW3_mhannah2"
author: "Hannah Most"
date: "2/7/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(tidyverse)
library(dplyr)
```

# Problem 4

## Part A.

I don't have too many initial thoughts. This definitely looks like a regression output.

X has a standard deviation of 3.32, and 2.03 for Y. The mean of X is 9, whereas the mean of Y is 7.5. It seems a little weird that output doesn't change instance-to-instance; I'm not sure why that is.

## Part B.

The linear model is at least adjusting per instance. I would say it's a pretty okay fit for at least some of the 

## Part C.

I would say that the lesson it to not assume linearity across all datasets. While the "1"s and "3"s look [at least mostly] linear, 2 and 4 certainly do not. The regressions are majorly different when you see them ploted, but it is not obvious up until that point.

# Problem 5

## Part A

```{r}
library(tidyverse)
library(dplyr)
library(readr)
    url<-"https://www2.isye.gatech.edu/~jeffwu/wuhamadabook/data/ThicknessGauge.dat"
    A_raw<-read.table(url, header=F, skip=1, fill=T, stringsAsFactors = F)
    A_tidy<-A_raw[-1,]
    colnames(A_tidy)<-c("Part","o1_r1", "o1_r2", "o2_r1", "o2_r2", "o3_r1", "o3_r2")
    A_tidy2<-A_tidy %>%  
        gather(o_r,value, o1_r1:o3_r2) %>%  
        separate(o_r,into=c("operator","replicate"),sep="_") %>%  
        mutate(operator = gsub("o","",operator)) %>%  
        #mutate(replicate = as.numeric(replicate)) %>%  
        mutate(Part = as.numeric(Part)) 
```

```{r}
knitr::kable(summary(A_tidy2), caption="Operator Data Summary")
```

```{r}  
boxplot(value~operator, data=A_tidy2, xlab="Operator", ylab="Measurement")
```

## Part B

```{r}
url2<-"https://www2.isye.gatech.edu/~jeffwu/wuhamadabook/data/LarvaeControl.dat"
B_raw<-read.table(url2, header=F, skip=1, fill=T, stringsAsFactors = F)
B_tidy <- B_raw[-1,] 
    
#B_tidy$v2 <- as.numeric(as.character(B_tidy$v2))
colnames(B_tidy)<- c("Block","a1_t1", "a1_t2", "a1_t3", "a1_t4", "a1_t5", "a2_t1", "a2_t2", "a2_t3", "a2_t4", "a2_t5") 
B_tidy <- B_tidy %>%
gather(age_treatment, value, a1_t1:a2_t5) %>%  
separate(age_treatment,into=c("age","treatment"),sep="_") %>%  
mutate(age = gsub("Age","",age)) %>%  
mutate(treatment = gsub("Treatment", "", treatment)) %>%  
mutate(Block = as.numeric(Block)) 
```

```{r}
knitr::kable(summary(B_tidy), caption="Larvae Data Summary")
```

```{r}  
#boxplot(treatment~age, data=B_tidy)
#legend("topleft", legend=levels(as.factor(B_tidy$age)), pch=20, col=levels(as.factor(B_tidy$age)))
#boxplot(value~treatment, data=B_tidy, xlab="Treatment", ylab="Age")
```